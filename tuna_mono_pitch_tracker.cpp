#include "tuna_mono_pitch_tracker.hpp"

TunaMonoPitchTracker::TunaMonoPitchTracker(double samplerate) {
    //If called without param, use default samplerate of 48000
    this->samplerate = samplerate;

    this->rms_omega = 1.0f - expf(-2.0 * M_PI * 15.0 / samplerate);

    /* initialize Thresholds */
    this->t_rms = -75.0f; // range: -100.0f -  0.0f
    this->t_flt = -45.0f; // range: - 50.0f -  0.0f
    this->t_fft = -40.0f; // range: - 50.0f - 10.0f
    this->t_ovr =  20.0f; // range:    0.0f - 40.0f
    this->t_fun =   5.0f; // range:    0.0f - 60.0f
    this->t_oct = -30.0f; // range: -100.0f -  0.0f
    this->t_ovt = -15.0f; // range: -100.0f -  0.0f

    this->rms_threshold = powf(10.0f, 0.1f * t_rms);
    this->flt_threshold = powf(10.0f, 0.1f * t_flt);
    this->fft_threshold = powf(10.0f, 0.1f * t_fft);
    this->ovr_threshold = powf(10.0f, 0.1f * t_ovr);
    this->fun_threshold = powf(10.0f, 0.1f * t_fun);
    this->oct_threshold = powf(10.0f, 0.1f * t_oct);
    this->ovt_threshold = powf(10.0f, 0.1f * t_ovt);

    /* initialize FFT */
    this->fftx = (struct FFTAnalysis*) calloc(1, sizeof(struct FFTAnalysis));
    int fft_size;
    fft_size = MAX(6144, samplerate / 15);

    /* round up to next power of two */
    fft_size--;
    fft_size |= fft_size >> 1;
    fft_size |= fft_size >> 2;
    fft_size |= fft_size >> 4;
    fft_size |= fft_size >> 8;
    fft_size |= fft_size >> 16;
    fft_size++;
    fft_size = MIN(32768, fft_size);

    fftx_init(this->fftx, fft_size, samplerate, 0);

    /* Midi filter buffer */
    previous_notes = new int[this->previous_notes_size]();
}


TunaMonoPitchTracker::~TunaMonoPitchTracker() {
    delete [] previous_notes;
    previous_notes = nullptr;

    fftx_free(this->fftx);
}

int TunaMonoPitchTracker::process(const float* input, uint32_t frames) {

	/* localize variables */
	float freq = this->tuna_fc;

	/* initialize local vars */
	float    detected_freq = 0;
	uint32_t detected_count = 0;
	bool fft_ran_this_cycle = false;
	bool fft_proc_this_cycle = false;
	bool fft_active = true;

    fft_ran_this_cycle = 0 == fftx_run(this->fftx, frames, input);


	/* process every sample */
	for (uint32_t n = 0; n < frames; ++n) {

		/* 1) calculate RMS */
		this->rms_signal += this->rms_omega * ((input[n] * input[n]) - this->rms_signal) + 1e-20;

		if (this->rms_signal < this->rms_threshold) {
			/* signal below threshold */
			this->dll_initialized = false;
			this->fft_initialized = false;
			this->fft_note_count = 0;
			this->prev_smpl = 0;
			continue;
		}

		/* 2) detect frequency to track
		 * use FFT to roughly detect the area
		 */

		/* FFT accumulates data and only returns us some
		 * valid data once in a while.. */
		if (fft_ran_this_cycle && !fft_proc_this_cycle) {
			fft_proc_this_cycle = true;
			/* get lowest peak frequency */
			const float fft_peakfreq = fftx_find_note(this->fftx, this->rms_signal * this->fft_threshold, this->ovr_threshold, this->fun_threshold, this->oct_threshold, this->ovt_threshold);
			if (fft_peakfreq < 20) {
				this->fft_note_count = 0;
			} else {
				const float note_freq = freq_to_scale(fft_peakfreq, NULL);
				/* keep track of fft stability */
				if (note_freq == this->fft_scale_freq) {
					this->fft_note_count+=frames;
				} else {
					this->fft_note_count = 0;
				}
				this->fft_scale_freq = note_freq;

			//	debug_printf("FFT found peak: %fHz -> freq: %fHz (%d)\n", fft_peakfreq, note_freq, this->fft_note_count);

				if (freq != note_freq &&
						(   (!this->dll_initialized && this->fft_note_count > 768)
						 || (this->fft_note_count > 1536 && fabsf(freq - note_freq) > MAX(FFT_FREQ_THESHOLD_MIN, freq * FFT_FREQ_THESHOLD_FAC))
						 || (this->fft_note_count > this->samplerate / 8)
						)
					 ) {
				//	info_printf("FFT adjust %fHz -> %fHz (fft:%fHz) cnt:%d\n", freq, note_freq, fft_peakfreq, this->fft_note_count);
					freq = note_freq;
				}
			}
		}

		/* refuse to track insanity */
		if (freq < 20 || freq > 10000 ) {
			this->dll_initialized = false;
			this->prev_smpl = 0;
			continue;
		}

		/* 2a) re-init detector coefficients with frequency to track */
		if (freq != this->tuna_fc) {
			this->tuna_fc = freq;
			//info_printf("set filter: %.2fHz\n", freq);

			/* calculate DLL coefficients */
			const double omega = ((this->tuna_fc < 50) ? 6.0 : 4.0) * M_PI * this->tuna_fc / this->samplerate;
			this->dll_b = 1.4142135623730950488 * omega; // sqrt(2)
			this->dll_c = omega * omega;
			this->dll_initialized = false;

			/* re-initialize filter */
			bandpass_setup(&this->fb, this->samplerate, this->tuna_fc
					, MAX(10, this->tuna_fc * .10)
					, 4 /*th order butterworth */);
			this->filter_init = 16;
		}

		/* 3) band-pass filter the signal to clean up the
		 * waveform for counting zero-transitions.
		 */
		const float signal = bandpass_process(&this->fb, input[n]);

		if (this->filter_init > 0) {
			this->filter_init--;
			this->rms_postfilter = 0;
			continue;
		}

		/* 4) reject signals outside in the band */
		this->rms_postfilter += this->rms_omega * ( (signal * signal) - this->rms_postfilter) + 1e-20;
		if (this->rms_postfilter < this->rms_signal * this->flt_threshold) {
			// debug_printf("signal too low after filter: %f %f\n",
			//		10.*fast_log10(2.f *this->rms_signal),
			//		10.*fast_log10(2.f *this->rms_postfilter));
			this->dll_initialized = false;
			this->prev_smpl = 0;
			continue;
		}

		/* 5) track phase by counting
		 * rising-edge zero-transitions
		 * and a 2nd order phase-locked loop
		 */
		if (   (signal >= 0 && this->prev_smpl < 0)
#ifdef TWO_EDGES
				|| (signal <= 0 && this->prev_smpl > 0)
#endif
				) {

			if (!this->dll_initialized) {
				//info_printf("reinit DLL\n");
				/* re-initialize DLL */
				this->dll_initialized = true;
				this->dll_e0 = this->dll_t0 = 0;
#ifdef TWO_EDGES
				this->dll_e2 = this->samplerate / this->tuna_fc / 2.f;
#else
				this->dll_e2 = this->samplerate / this->tuna_fc;
#endif
				this->dll_t1 = this->monotonic_cnt + n + this->dll_e2;
			} else {
				/* phase 'error' = detected_phase - expected_phase */
				this->dll_e0 = (this->monotonic_cnt + n) - this->dll_t1;

				/* update DLL, keep track of phase */
				this->dll_t0 = this->dll_t1;
				this->dll_t1 += this->dll_b * this->dll_e0 + this->dll_e2;
				this->dll_e2 += this->dll_c * this->dll_e0;

#ifdef TWO_EDGES
				const float dfreq0 = this->samplerate / (this->dll_t1 - this->dll_t0) / 2.f;
				const float dfreq2 = this->samplerate / (this->dll_e2) / 2.f;
#else
				const float dfreq0 = this->samplerate / (this->dll_t1 - this->dll_t0);
				const float dfreq2 = this->samplerate / (this->dll_e2);
#endif
				// debug_printf("detected Freq: %.2f flt: %.2f (error: %.2f [samples]) diff:%f)\n", dfreq0, dfreq2, this->dll_e0, (this->dll_t1 - this->dll_t0) - this->dll_e2);

				float dfreq;
				if (fabsf (this->dll_e0 * freq / this->samplerate) > .02) {
					dfreq = dfreq0;
				} else {
					dfreq = dfreq2;
				}

#if 1
				/* calculate average of all detected values in this cycle.
				 * this is questionable, just use last value.
				 */
				detected_freq += dfreq;
				detected_count++;
#else
				detected_freq = dfreq;
				detected_count= 1;
#endif
			}
		}
		this->prev_smpl = signal;
	} // end of for loop (process every sample)

	if (!this->dll_initialized) {
		this->monotonic_cnt = 0;
	} else {
		this->monotonic_cnt += frames;
	}


	/* post-processing and data-output */
	if (detected_count > 0) {
        int note = -1;

		/* calculate average of detected frequency */
		const float freq_avg = detected_freq / (float)detected_count;
		/* ..and the corresponding note on the scale */
		const float note_freq = freq_to_scale(freq_avg, &note);

		// debug_printf("detected Freq: %.2f (error: %.2f [samples])\n", freq_avg, this->dll_e0);

		/* calculate cent difference
		 * One cent is one hundredth part of the semitone in 12-tone equal temperament
		 */
		const float cent = 1200.0 * log2(freq_avg / note_freq);

		bool cent_diff = false;
		bool note_diff = false;

		if (fabsf (this->cent_last - cent) > .05) {
			cent_diff = true;
			this->cent_last = cent;
		}
		if (this->note_last != note) {
			note_diff = true;
			this->note_last = note;
		}

		if (note_diff || cent_diff) {
			mts (note, cent);
		}

        updateArr_fifo(note, this->previous_notes, this->previous_notes_size);
        int pitch_mode = calc_statistical_mode(this->previous_notes,this->previous_notes_size);
        if(pitch_mode != this->previous_pitch_mode) {
            this->previous_pitch_mode = pitch_mode;
            return previous_pitch_mode;
        }

    }
    else if (!this->dll_initialized) {
        /* no signal detected; or below threshold */
	}
	/* else { no change, maybe short cycle } */

    return -1;

} //process()

void TunaMonoPitchTracker::setSamplerate(double samplerate) {
    //TODO: add parameter protection
    this->samplerate = samplerate;
    //Add samplerate updates, like fft_init and rms_omega.
}

/* recursively scan octave-overtones up to 4 octaves */
uint32_t TunaMonoPitchTracker::fftx_scan_overtones(struct FFTAnalysis *ft,
		const float threshold, uint32_t bin, uint32_t octave,
		const float v_oct2)
{
	const float scan  = MAX(2, (float) bin * .1f);
	uint32_t peak_pos = 0;
	for (uint32_t i = MAX(1, floorf(bin-scan)); i < ceilf(bin+scan); ++i) {
		if (
				   ft->power[i] > threshold
				&& ft->power[i] > ft->power[i-1]
				&& ft->power[i] > ft->power[i+1]
			 ) {
			peak_pos = i;
			//debug_printf("ovt: bin %d oct %d th-fact: %f\n", i, octave, 10.0 * fast_log10(ft->power[i]/ threshold));
			break;
		}
	}
	if (peak_pos > 0) {
		octave *= 2;
		if (octave <= 16) {
			octave = fftx_scan_overtones(ft, threshold * v_oct2, peak_pos * 2, octave, v_oct2);
		}
	}
	return octave;
}

/** find lowest peak frequency above a given threshold */
float TunaMonoPitchTracker::fftx_find_note(struct FFTAnalysis *ft,
		const float abs_threshold,
		const float v_ovr, const float v_fun, const float v_oct, const float v_ovt)
{
	uint32_t fundamental = 0;
	uint32_t octave = 0;
	float peak_dat = 0;
	const uint32_t brkpos = ft->data_size * 8000 / ft->rate;
	float threshold = abs_threshold;

	for (uint32_t i = 1; i < brkpos; ++i) {
		if (
				ft->power[i] > threshold
				&& ft->power[i] > ft->power[i-1]
				&& ft->power[i] > ft->power[i+1]
			 ) {

			uint32_t o = fftx_scan_overtones(ft, ft->power[i] * v_oct, i * 2, 2, v_ovt);
			// debug_printf("Candidate (%d) %f Hz -> %d overtones\n", i, fftx_freq_at_bin(ft, i) , o);

			if (o > octave
					|| (ft->power[i] > threshold * v_ovr)
					) {
				if (ft->power[i] > peak_dat) {
					peak_dat = ft->power[i];
					fundamental = i;
					octave = o;
					/* only prefer higher 'fundamental' if it's louder than a /usual/ 1st overtone.  */
					if (o > 2) threshold = peak_dat * v_fun;
					//if (o > 16) break;
				}
			}
		}
	}

	//debug_printf("fun: bin: %d octave: %d freq: %.1fHz th-fact: %fdB\n",
//			fundamental, octave, fftx_freq_at_bin(ft, fundamental), 10 * fast_log10(threshold / abs_threshold));
	if (octave == 0) { return 0; }
	return fftx_freq_at_bin(ft, fundamental);
}

int TunaMonoPitchTracker::calc_statistical_mode(int arr[],int size) {
    int maxValue = 0, maxCount = 0, i, j;
    bool multimodal = false;

    for (i = 0; i < size; ++i) {
        int count = 0;

        if (arr[i] != maxValue || i == 0) {
            for (j = 0; j < size; ++j) {
                if (arr[j] == arr[i])
                    ++count;
            }

            if (count > maxCount) {
                maxCount = count;
                maxValue = arr[i];
            }
            if (count == maxCount && arr[i] != maxValue) {
                multimodal = true;
            }
        }
    }
    //TODO: do something when the dataset is multimodal.
    return maxValue;
}

void TunaMonoPitchTracker::updateArr_fifo(int value, int arr[], unsigned int size) {
    for(unsigned int i = 0; i < size; i++) {
        if(i < size - 1) {
            arr[i] = arr[i+1];
        } else {
            arr[i] = value;
        }
    }
}

void TunaMonoPitchTracker::mts (const int note, const float cent) {
	if ((note < 0 || note > 127) || (note == 0 && cent < 0)) {
		return;
	}

	if (cent < -50.f || cent > 50.f) {
		return;
	}

	uint8_t nn = note & 127;
	uint16_t cc = floorf (163.83 * ((cent >= 0) ? cent : (100.f - cent)));

	// http://www.microtonal-synthesis.com/MIDItuning.html
	// http://technogems.blogspot.com/2018/07/using-midi-tuning-specification-mts.html
}

/* round frequency to the closest note on the given scale.
 * use midi notation 0..127 for note-names
 */
float TunaMonoPitchTracker::freq_to_scale(const float freq, int *midinote) {
	/* calculate corresponding note - use midi notation 0..127 */
	const int note = rintf(12.f * log2f(freq / this->tuning) + 69.0);
	if (midinote) *midinote = note;
	/* ..and round it back to frequency */
	return this->tuning * powf(2.0, (note - 69.f) / 12.f);
}
