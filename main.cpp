#include <iostream>

#include "tuna_mono_pitch_tracker.hpp"

int main() {
    TunaMonoPitchTracker tuna(44100);

    std::cout << "Hello Tuna" << std::endl;
    return 0;
}
