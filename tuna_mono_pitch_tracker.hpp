#ifndef TUNAMONOPITCHTRACKER_H
#define TUNAMONOPITCHTRACKER_H

#include "fft.c"
#include "spectr.c"


#ifndef MIN
#define MIN(A,B) ( (A) < (B) ? (A) : (B) )
#endif
#ifndef MAX
#define MAX(A,B) ( (A) > (B) ? (A) : (B) )
#endif

/* use FFT signal if tracked freq & FFT-freq differ by more than */
#define FFT_FREQ_THESHOLD_FAC (.10f)
/* but at least .. [Hz] */
#define FFT_FREQ_THESHOLD_MIN (5.f)

/* use both rising and falling signal edge to track phase */
#define TWO_EDGES

/* debug */
// #if 0
// #define info_printf printf
// #else
// void info_printf (const char *fmt,...) {}
// #endif

// #if 0 // lots of output
// #define debug_printf printf
// #else
// void debug_printf (const char *fmt,...) {}
// #endif


class TunaMonoPitchTracker {
public:
    TunaMonoPitchTracker(double samplerate =48000);
    ~TunaMonoPitchTracker();

    int process(const float* input, uint32_t frames);

    void setSamplerate(double samplerate);

private:

    /* Tuning frequency */
    float tuning = 440.0f; // origianl intended range: 220-880 Hz

    /* internal state */
    double samplerate;
    struct FilterBank fb;
    float tuna_fc           = 0.0f; // center freq of expected note
    uint32_t filter_init;

    /* rate limit */
    int   note_last         = -1;
    float cent_last         = 0.0f;

    /* discriminator */
    float prev_smpl         = 0.0f;

    /* RMS / threshold */
    float rms_omega;
    float rms_signal        = 0.0f;
    float rms_postfilter    = 0.0f;
    float rms_last          = -100.0f;

    /* Thresholds */
    float t_rms, rms_threshold;
    float t_flt, flt_threshold;
    float t_fft, fft_threshold;
    float t_ovr, ovr_threshold;
    float t_fun, fun_threshold;
    float t_oct, oct_threshold;
    float t_ovt, ovt_threshold;

    /* Delay Locked Loop (DLL) */
    bool dll_initialized = false;
    uint32_t monotonic_cnt;
    double dll_e0, dll_e2 = 0.0f;
    double dll_t0, dll_t1 = 0.0f;
    double dll_b, dll_c;

    /* FFT */
    struct FFTAnalysis *fftx;
    bool fft_initialized    = false;
    float fft_scale_freq    = 0.0f;
    int fft_note_count      = 0;

    uint32_t fftx_scan_overtones(struct FFTAnalysis *ft, const float threshold,
                                 uint32_t bin, uint32_t octave, const float v_oct2);
    float fftx_find_note(struct FFTAnalysis *ft, const float abs_threshold,
                         const float v_ovr,      const float v_fun,
                         const float v_oct,      const float v_ovt);

    /* Midi filtering */
    int previous_pitch_mode = 0;
    const int previous_notes_size = 12;
    int* previous_notes;

    int calc_statistical_mode(int arr[],int size);
    void updateArr_fifo(int value, int arr[], unsigned int size);

    /* Other functions */
    void mts (const int note, const float cent); //useless. Old use: send data with LV2 atom
    float freq_to_scale(const float freq, int *midinote);

};

#endif
