PRGRM = tuna_mono_pitch_tracker
OBJ = main.o tuna_mono_pitch_tracker.o fft.o spectr.o

CXXFLAGS := -Wall -std=c++14 -g
LDFLAGS = 
LDLIBS = -lfftw3 -lfftw3f -lm

all: $(PRGRM)

# link the program
$(PRGRM): $(OBJ)
	$(CXX) -o $@ $(CXXFLAGS) $(OBJ) $(OBJ_C) $(LDFLAGS) $(LDLIBS)

# builds given .o files dependend on their corresponding .cpp and .h files
%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(CPPFLAGS) $< -o $@

clean:
	rm $(OBJ)
	rm $(PRGRM)

.PHONY: all clean

